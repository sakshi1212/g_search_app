require 'open-uri'
class Csv < ActiveRecord::Base
  belongs_to :user
  has_many :keywords

  def google_search keyword
    Resque.enqueue(GoogleSearchKeyword, :do_search, {id: self.id, keyword: keyword})
  end

end
