class Url < ActiveRecord::Base

  belongs_to :keyword
  enum url_type: [TOP_AD, BOTTOM_AD, NON_AD]

end
