class ReportsController < ApplicationController

  def new
  end

  def create
    timestamp = DateTime.now
    filename, report_data = generate_report_csv timestamp
    Report.create(user_id: current_user.id, title: filename, data: report_data)
  end

  def index
    @reports = Report.all.order(id: :desc).includes(:user)
  end

  def show
    @report = Report.find params[:id]
    @headers = @report.data["0"].keys.map!(&:humanize)
    @data = @report.data.except!("0")
  end

  private

  def generate_report_csv timestamp
    filename = "keyword-report-#{timestamp}"
    base_qry = Keyword.joins('LEFT JOIN csvs ON csvs.id=keywords.csv_id')
                      .select("keywords.id as keyword_id, csvs.id as csv_id, csvs.filename as csv_filename, keywords.word as keyword, keywords.no_of_ads_top, keywords.no_of_ads_bottom, keywords.no_of_total_ads, keywords.no_of_non_ads, keywords.no_of_links, keywords.total_results, keywords.created_at")
    query = base_qry.reorder(word: :asc).references(:csv).to_sql
    response.headers["Cache-Control"] = "no-cache"
    response.headers["Content-Type"] = "text/csv; charset=utf-8"
    response.headers["Content-Disposition"] =  %(attachment; filename="#{filename}")
    response.headers["Last-Modified"] = Time.zone.now.ctime.to_s
    conn = ActiveRecord::Base.connection.raw_connection
    full_data = []
    conn.copy_data("COPY (#{query}) TO STDOUT WITH (FORMAT CSV, HEADER TRUE, FORCE_QUOTE *, ESCAPE E'\\\\');") do
       while row = conn.get_copy_data
        response.stream.write row
        ret_hash = row_to_hash row
        full_data<<ret_hash
       end
     end
     response.stream.close
     full_hash = {}
     full_data.each_with_index {|val,index| full_hash[index]=val}
     return filename, full_hash.as_json
  end

  def row_to_hash row
    val_array = row.split(',')
    headers = ['keyword_id', 'csv_id', 'filename', 'word', 'no_of_ads_top', 'no_of_ads_bottom', 'no_of_total_ads', 'no_of_non_ads', 'no_of_links', 'no_of_total_results', 'created_at']
    val_hash = {}
    headers.each_with_index do |h, i|
      val_hash[h.to_sym] = val_array[i]
    end
    val_hash
  end

end
