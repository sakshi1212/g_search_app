class KeywordsController < ApplicationController

  def show
    @keyword = Keyword.find params[:id].to_i
    @keyword_urls = @keyword.urls.order(:url_type)
  end

end
