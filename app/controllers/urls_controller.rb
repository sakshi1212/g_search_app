class UrlsController < ApplicationController

  def index
    @q = Url.ransack(params[:q])
    @results = @q.result.includes(:keyword)
  end

end
