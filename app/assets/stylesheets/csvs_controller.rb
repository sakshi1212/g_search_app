class CsvsController < ApplicationController

  def new
  end

  def create
    f_name, f_data = read_csv params[:csv][:file]
    csv = Csv.create(user_id: current_user.id, filename: f_name)
    f_data.each do |kw|
      csv.google_search kw
    end
    redirect_to csvs_path
  end

  def index
    @all_csvs = Csv.joins(:user)
  end

  def show
    @csv = Csv.find params[:id].to_i
    @keywords = @csv.keywords
  end

  private
  def read_csv file
    csv_data = CSV.read(file.path)
    csv_name = file.original_filename
    return csv_name, csv_data
  end

end
