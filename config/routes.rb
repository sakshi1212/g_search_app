Rails.application.routes.draw do
  root to: "csvs#new"
  resources :csvs, :only => [:new, :create, :index, :show]
  resources :keywords, :only => [:show]
  resources :reports, :only => [:new, :create, :index, :show]
  resources :urls, :only => [:index]
  mount Resque::Server.new, at: '/debug/resque'
  devise_for :users

end
