require 'open-uri'

module SearchKeyword

  def self.do_search(opts={})
    keyword = opts['keyword']
    csv_id = opts['id'].to_i
    csv = Csv.find csv_id
    if csv
      doc = Nokogiri::HTML(open("http://www.google.com/search?q=#{keyword}"))
      ads = doc.css('.C4eCVc')
      if ads.present?
        top_ads = ads.first.css('.ads-ad')
        no_of_top_ads = top_ads.size
        bottom_ads = ads.last.css('.ads-ad')
        no_of_bottom_ads = bottom_ads.size
        total_ads = doc.css('.ads-ad')
        no_of_total_ads = total_ads.size
        top_ads_cites = top_ads.css(".ads-visurl>cite")
        top_ads_urls = top_ads_cites.map{|url| url.children.text()}
        bottom_ads_cites = bottom_ads.css(".ads-visurl>cite")
        bottom_ads_urls = bottom_ads_cites.map{|url| url.children.text()}
      end
      non_ad_results = doc.css('#search .g')
      no_of_non_ad_results = non_ad_results.size
      non_ad_cites = non_ad_results.css('cite')
      non_ad_urls = non_ad_cites.map{|url| url.children.text()}
      total_links = doc.css('a').size
      result_stats = doc.css('#resultStats').text.strip.split(' ')[1].gsub(',','').to_i
      kw_record = csv.keywords.create(word: keyword,
                            no_of_ads_top: no_of_top_ads,
                            no_of_ads_bottom: no_of_bottom_ads,
                            no_of_total_ads: no_of_total_ads,
                            no_of_non_ads: no_of_non_ad_results,
                            no_of_links: total_links,
                            total_results: result_stats)
      urls_arr = []
      non_ad_urls.each {|u| urls_arr << Url.new(keyword_id: kw_record.id, url_type: NON_AD, url_link: u)} if non_ad_urls.present?
      bottom_ads_urls.each {|u| urls_arr << Url.new(keyword_id: kw_record.id, url_type: BOTTOM_AD, url_link: u)} if bottom_ads_urls.present?
      top_ads_urls.each {|u| urls_arr << Url.new(keyword_id: kw_record.id, url_type: TOP_AD, url_link: u)} if top_ads_urls.present?
      Url.import urls_arr
    end
  end

end
