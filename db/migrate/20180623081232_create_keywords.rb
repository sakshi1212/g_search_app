class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.integer :csv_id
      t.string :word
      t.integer :no_of_ads_top
      t.integer :no_of_ads_bottom
      t.integer :no_of_total_ads
      t.integer :no_of_non_ads
      t.integer :no_of_links
      t.integer :total_results

      t.timestamps null: false
    end
  end
end
