class CreateUrls < ActiveRecord::Migration
  def change
    create_table :urls do |t|
      t.integer :keyword_id
      t.integer :url_type
      t.string :url_link

      t.timestamps null: false
    end
  end
end
