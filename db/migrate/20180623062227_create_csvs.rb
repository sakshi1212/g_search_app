class CreateCsvs < ActiveRecord::Migration
  def change
    create_table :csvs do |t|
      t.string :filename
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
