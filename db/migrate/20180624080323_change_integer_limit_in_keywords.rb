class ChangeIntegerLimitInKeywords < ActiveRecord::Migration
  def change
    change_column :keywords, :total_results, :integer, limit: 8
  end
end
