class AddDataToReports < ActiveRecord::Migration
  def change
    add_column :reports, :data, :json
  end
end
