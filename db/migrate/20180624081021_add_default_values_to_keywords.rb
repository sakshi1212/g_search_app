class AddDefaultValuesToKeywords < ActiveRecord::Migration
  def change
    change_column :keywords, :no_of_ads_top, :integer, default: 0
    change_column :keywords, :no_of_ads_bottom, :integer, default: 0
    change_column :keywords, :no_of_total_ads, :integer, default: 0
    change_column :keywords, :no_of_non_ads, :integer, default: 0
    change_column :keywords, :no_of_links, :integer, default: 0
    change_column :keywords, :total_results, :integer, default: 0, limit: 8
  end
end
